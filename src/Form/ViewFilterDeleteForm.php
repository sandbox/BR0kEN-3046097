<?php

namespace Drupal\views_save\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\EventSubscriber\AjaxResponseSubscriber;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfirmFormHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The "delete" form handler of the "view_filter" entity type.
 *
 * @property \Drupal\views_save\Entity\ViewFilterInterface $entity
 * @method \Drupal\views_save\Entity\ViewFilterInterface getEntity()
 *
 * @link https://www.drupal.org/project/drupal/issues/2253257
 */
class ViewFilterDeleteForm extends ContentEntityDeleteForm {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * An instance of the "MODULE.view_filters_markup" service.
   *
   * @var \Drupal\views_save\ViewFiltersMarkup
   */
  protected $viewFiltersMarkup;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);
    assert($instance instanceof self);
    $instance->requestStack = $container->get('request_stack');
    $instance->currentRequest = $instance->requestStack->getCurrentRequest();
    /* @noinspection NullPointerExceptionInspection */
    $instance->viewFiltersMarkup = $container
      ->get('views_save.view_filters_markup')
      ->setCaller(static::class);

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state): array {
    if (!isset($form_state->getUserInput()['confirm'])) {
      $this->viewFiltersMarkup->setAjaxResponse($this->currentRequest->request->has(AjaxResponseSubscriber::AJAX_REQUEST_PARAMETER));
    }

    return [
      'submit' => $this->getSubmitButton(),
      'cancel' => $this->getCancelButton(),
    ];
  }

  /**
   * Returns the renderable representation of the "submit" button.
   *
   * @return array
   *   The "submit" button.
   */
  protected function getSubmitButton(): array {
    $button = [
      '#type' => 'submit',
      '#value' => $this->getConfirmText(),
    ];

    if ($this->viewFiltersMarkup->isAjaxResponse()) {
      $button['#type'] = 'button';
      $button['#ajax']['callback'] = '::submitForm';
    }

    return $button;
  }

  /**
   * Returns the renderable representation of the "cancel" button.
   *
   * @return array
   *   The "cancel" button.
   */
  protected function getCancelButton(): array {
    $button = ConfirmFormHelper::buildCancelLink($this, $this->currentRequest);
    $button['#attributes']['class'][] = 'dialog-cancel';

    return $button;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->viewFiltersMarkup->isAjaxResponse()) {
      $this->entity->delete();
      $this->logDeletionMessage();

      return $this->viewFiltersMarkup->attachMarkup(
        new AjaxResponse(),
        $this->entity->getViewId(),
        $this->entity->getViewDisplayId(),
        $this->getDeletionMessage()
      );
    }

    parent::submitForm($form, $form_state);

    return NULL;
  }

}
