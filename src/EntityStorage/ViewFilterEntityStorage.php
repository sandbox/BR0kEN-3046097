<?php

namespace Drupal\views_save\EntityStorage;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\views\Plugin\views\cache\CachePluginBase;
use Drupal\views_save\Entity\ViewFilterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The "storage" handler of the "view_filter" entity type.
 *
 * @method ViewFilterInterface load($id)
 * @method ViewFilterInterface[] loadMultiple(array $ids = NULL)
 * @method ViewFilterInterface[] loadByProperties(array $values = [])
 * @method ViewFilterInterface[] loadMultipleRevisions(array $revision_ids)
 */
class ViewFilterEntityStorage extends SqlContentEntityStorage {

  /**
   * An instance of the "current_user" service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * An instance of the "MODULE.entity_id_uuid_exchanger" service.
   *
   * @var \Drupal\views_save\EntityIdForUuidExchanger
   */
  protected $entityIdForUuidExchanger;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): self {
    $instance = parent::createInstance($container, $entity_type);
    assert($instance instanceof self);
    $instance->currentUser = $container->get('current_user');
    $instance->entityIdForUuidExchanger = $container->get('views_save.entity_id_uuid_exchanger');

    return $instance;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function buildQuery($ids, $revision_ids = FALSE): SelectInterface {
    return parent
      ::buildQuery($ids, $revision_ids)
        ->condition('user', $this->entityIdForUuidExchanger->exchange('user', $this->currentUser->id()));
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function doPostSave(EntityInterface $entity, $update): void {
    parent::doPostSave($entity, $update);
    assert($entity instanceof ViewFilterInterface);
    static::invalidateViewDisplayCache($entity);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function delete(array $entities): void {
    parent::delete($entities);

    foreach ($entities as $entity) {
      assert($entity instanceof ViewFilterInterface);
      static::invalidateViewDisplayCache($entity);
    }
  }

  /**
   * Invalidates the cache of a view display.
   *
   * @param \Drupal\views_save\Entity\ViewFilterInterface $view_filter
   *   The entity to handle.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected static function invalidateViewDisplayCache(ViewFilterInterface $view_filter): void {
    $cache_plugin = $view_filter
      ->getViewExecutable()
      ->getDisplay()
      ->getPlugin('cache');

    if ($cache_plugin !== NULL) {
      assert($cache_plugin instanceof CachePluginBase);
      $cache_plugin->cacheFlush();
    }
  }

}
