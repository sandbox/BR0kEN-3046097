<?php

namespace Drupal\views_save\AccessControl;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\views_save\Entity\ViewFilterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The "access" handler of the "view_filter" entity type.
 */
class ViewFilterAccessControl extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * An instance of the "MODULE.entity_id_uuid_exchanger" service.
   *
   * @var \Drupal\views_save\EntityIdForUuidExchanger
   */
  protected $entityIdForUuidExchanger;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): self {
    if (method_exists(parent::class, __FUNCTION__)) {
      $instance = parent::createInstance($container, $entity_type);
    }
    else {
      $instance = new static($entity_type);
    }

    assert($instance instanceof self);
    $instance->entityIdForUuidExchanger = $container->get('views_save.entity_id_uuid_exchanger');

    return $instance;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    assert($entity instanceof ViewFilterInterface);
    $account = $this->prepareUser($account);

    if (
      $account->hasPermission('administer all views filters') ||
      (
        $account->hasPermission('use own views filters') &&
        $entity->getUserUuid() === $this->entityIdForUuidExchanger->exchange('user', $account->id())
      )
    ) {
      return $return_as_object ? AccessResult::allowed() : TRUE;
    }

    return $return_as_object ? AccessResult::forbidden() : FALSE;
  }

}
