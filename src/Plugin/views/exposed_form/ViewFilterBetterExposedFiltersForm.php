<?php

namespace Drupal\views_save\Plugin\views\exposed_form;

use Drupal\better_exposed_filters\Plugin\views\exposed_form\BetterExposedFilters;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\modal_form\Element\ModalFormLink;
use Drupal\views_save\Entity\ViewFilter;
use Drupal\views_save\Form\ViewFilterSelectForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Extended "exposed_form" allowing to store configured filters.
 */
class ViewFilterBetterExposedFiltersForm extends BetterExposedFilters {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * An instance of the "current_user" service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * An instance of the "MODULE.view_filters_markup" service.
   *
   * @var \Drupal\views_save\ViewFiltersMarkup
   */
  protected $viewFiltersMarkup;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    assert($instance instanceof self);
    $instance->renderer = $container->get('renderer');
    $instance->viewFiltersMarkup = $container->get('views_save.view_filters_markup');
    $instance->currentUser = $container->get('current_user');
    /* @noinspection NullPointerExceptionInspection */
    $instance->currentRequest = $container
      ->get('request_stack')
      ->getCurrentRequest();

    return $instance;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function exposedFormAlter(&$form, FormStateInterface $form_state): void {
    parent::exposedFormAlter($form, $form_state);

    $route_name = $this->currentRequest->get('_route');
    $view_name = $this->view->id();
    $markup = $this->viewFiltersMarkup->getMarkup($view_name, $this->view->current_display);

    $form['#prefix'] = '<div class="view-filters--container">' . $this->renderer->renderPlain($markup);
    $form['#suffix'] = '</div>';

    if ($route_name === ModalFormLink::CONTROLLER_ROUTE || strpos($route_name, 'entity.view_filter.') === 0) {
      // Hide the "Apply" button. Removal is not allowed. Otherwise, the
      // values that are currently selected may be incorrectly treated.
      /* @noinspection UnsupportedStringOffsetOperationsInspection */
      $form['actions']['submit']['#printed'] = TRUE;
    }
    elseif (empty($_GET['hide-save-filter-button'])) {
      // Show the link to save current selection as a preset.
      $form['actions']['view_filter_save'] = [
        '#type' => 'modal_form_link',
        '#class' => ViewFilterSelectForm::class,
        '#title' => $this->t('Save filter'),
        '#access' => $this->currentUser->isAuthenticated(),
        '#printed' => !$this->isFiltersApplied(),
        '#attributes' => [
          'class' => ['save-filter'],
          'data-query-parameters' => Json::encode([
            ViewFilterSelectForm::PROP_VIEW_NAME => $view_name,
            ViewFilterSelectForm::PROP_VIEW_DISPLAY => $this->view->current_display,
          ]),
          'data-dialog-options' => Json::encode([
            'dialogClass' => 'modal--views-save',
            'width' => '500px',
          ]),
        ],
      ];
    }
  }

  /**
   * Returns a state whether the filters were applied.
   *
   * @return bool
   *   The state.
   */
  protected function isFiltersApplied(): bool {
    $input = $this->view->getExposedInput();

    if (empty($input) || !ViewFilter::isApplicable($this->view->display_handler, $this->currentUser)) {
      return FALSE;
    }

    return !empty(array_intersect_key(ViewFilter::getViewDisplayFilters($this->view->display_handler), $input));
  }

}
