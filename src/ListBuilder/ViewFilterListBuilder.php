<?php

namespace Drupal\views_save\ListBuilder;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\views_save\Entity\ViewFilterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The "list_builder" handler of the "view_filter" entity type.
 */
class ViewFilterListBuilder extends EntityListBuilder {

  /**
   * A storage of the "user" entities.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): self {
    $instance = parent::createInstance($container, $entity_type);
    assert($instance instanceof self);
    /* @noinspection NullPointerExceptionInspection */
    $instance->userStorage = $container
      ->get('entity_type.manager')
      ->getStorage('user');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header = [];

    $header['label'] = $this->t('Name');
    $header['user'] = $this->t('User');

    /* @noinspection AdditionOperationOnArraysInspection */
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function buildRow(EntityInterface $entity): array {
    assert($entity instanceof ViewFilterInterface);
    $accounts = $this->userStorage->loadByProperties(['uuid' => $entity->getUserUuid()]);
    $account = reset($accounts);
    assert($account instanceof UserInterface);
    $row = [];

    $row['label'] = [
      'data' => [
        '#type' => 'link',
        '#title' => $entity->label(),
        '#url' => $entity->getFiltersUrl(),
        '#attributes' => [
          'target' => '_blank',
        ],
      ],
    ];

    $row['user'] = [
      'data' => [
        '#type' => 'link',
        '#title' => $account->getAccountName(),
        '#url' => $account->toUrl(),
      ],
    ];

    /* @noinspection AdditionOperationOnArraysInspection */
    return $row + parent::buildRow($entity);
  }

}
